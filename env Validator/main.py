import json
import os

import helper
from dotenv import load_dotenv

load_dotenv()
env_data = os.environ.items()

file = open("data.json")
data = json.load(file)

for tc_file in data:
    res = helper.open_file(tc_file)
    check = helper.validate_env(env_data, res)
