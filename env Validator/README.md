# Env Validator

#### Problem Statement

```
Validate if all the env variables present in the dict are present and validate if it adheres to the conditions given.

Input:

{
    "version": {
        "type": "int",
        "min": 0,
        "max": 3
    },
    "name": {
        "type": "string",
        "min": null,
        "max": null
    },
    "allowed_hosts": {
        "type": "string",
        "min": null,
        "max": null
    }
}

Expected output:

VERSION=5 (Wrong version)
Name="" (Wrong Name)
allowed_hosts=""
```

#### Folder Structure

```
env Validator
    |-- .env
    |-- example.env
    |-- .gitignore
    |-- helper.py
    |-- data.json
    |-- main.py
    |-- expected_output.txt
    |-- README.md
    |-- requirements.txt
    |-- result.txt
    |
    |-- Tests
          |- TestCase_01.json
          |- TestCase_02.json
          |- TestCase_03.json
          |- TestCase_04.json
```

- `data.json` - List of test cases files.
- `main.py` - Main file.
- `expected_output.py` - List of expected output.
- `result.txt` - Result of the program will get stored.
- `Tests folder` - Contains test cases files.

Install required packages

```
pip install -r requirements.txt
```

To excute the program, run the below command

```
python main.py
```
