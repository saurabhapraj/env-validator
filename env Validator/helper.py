import json


def open_file(file) -> dict:
    file = open(f"Tests/{file}.json")
    data = json.load(file)
    return data


def write_in_file(message):
    file = open("result.txt", "a")
    file.writelines(message)
    file.writelines("\n")
    file.close()


def success_message(key: str, value: int | str):
    write_in_file(f"{key}={value}")


def failed_message(key, value):
    write_in_file(f"{key}={value} (Wrong {key})")


def validate_key(key: str, value: str, checker: dict):
    if type(value).__name__ == checker.get("type") or str:
        success_message(key, value)
    else:
        failed_message(key, value)

    if checker.get("max") != None and checker.get("min") != None:
        if value > checker.get("min"):
            success_message(key, value)
        else:
            failed_message(key, value)

        if value < checker.get("max"):
            success_message(key, value)
        else:
            failed_message(key, value)


def validate_env(env_data: dict, test_case_data: dict):
    for key, value in env_data:
        if key.lower() == "version":
            version = test_case_data.get("version")
            validate_key("version", int(value), version)

        elif key.lower() == "name":
            name = test_case_data.get("name")
            validate_key("name", value, name)

        elif key.lower() == "allowed_hosts":
            allowed_hosts = test_case_data.get("allowed_hosts")
            validate_key("allowed_hosts", value, allowed_hosts)


def check_files(expected_output_file, result_file):
    with open(expected_output_file, "rb") as file_1, open(result_file, "rb") as file_2:
        return file_1.read() == file_2.read()
